public class Intersection {
	public String name;
	public double longitude;
	public double latitude;	
  	public int index;      //use when constructing graphs. Useless otherwise
	public Boolean visited;
	public double distance;

	public Intersection(String name, double latitude, double longitude, int index){
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.index = index;
		visited = false;
		distance = Double.POSITIVE_INFINITY;
		
	}
}
