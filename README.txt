/* Name: Clara Martinez Rubio and Sakhile Mathunjwa
 * NetID: 28824213
 * Assignment number: Project 4
 * Lab section: MW 325-440
 * TA: Yiwen Fan, Olivia Morton
 * “We did not collaborate with anyone on this assignment"
 */

Synopsis:

	For Lab project 4, we have implemented a rudimentary mapping program in Java. The program reads formatted data on the intersections and roads that make up a map, creates a graph using that data, displays the graph using a GUI with Java Graphics, and calculates the shortest path between two intersections using Dijkstra’s algorithm. The final graph representation used is a HashMap of Strings to LinkedLists, where the Strings are IntersectionIDs and the LinkedList have a node Head, that stores the Intersection object, and a pointer to an Edge, which stores a Road that the intersection is a part of. All Edges have a pointer to the next Edge, thus forming the LinkedList. A priority queue is also implemented for run time purposes. When inserting the intersections into the graph, they are first inserted into a priority queue in order to find the shortest path. Finally, to display the entire Graph, every road is added to an ArrayList of Roads and a red line is painted with the same endpoints as the road. At the end, the program will also print the amount of time the algorithm took to compile. 
	The zip folder is called proj4.zip and the project folder is called Project04(CSC172). There are five classes named accordingly, Graph, Intersection, Road, SMapGUI, and StreetMap. The class StreetMap contains the main method.
//array of objects and array of edges

Notable Obstacles:
	
	The most notable obstacle that we had to overcome while doing this project was conceptualizing how to go about the project. We met up several days before we started writing code, first to think about what was the best implementation and then to divide the work. When we started to implement the project we came across the first dilemma: to either use an matrix or an adjacency list. At first, we were going to use a matrix like we did in lab 11, but learning more about that data structure, we realized that the program would run out of memory since it would have to store all the 0’s for the nodes that do not have a connecting edge. Using the min function for Dijkstras algorithm for a big data sets, like NY state, took around 5 minutes. So we decided on our current implementation, a priority queue, making this small adjustment decreased the run time to under a minute for any point.

Runtime analysis:

Plotting the map:
	In order to display the final graph, the are two arrays, an array of objects, and an array of edges, which are iterated through to graph every road. Therefore, the runtime for the graph display is O(|V|) where N is the number of roads in the given map.
Finding the shortest path between 2 intersections:
	After our latest implementation of the priority queue, the runtime for displaying the directions between two intersections is O(|E|log(|V|)) since we implemented the alogorithm using a priority queue.

	The overall runtime of the project will depend on the size of the map to be drawn and the distance between the points that you want to map.

Command line instructions:

Simply display the map:
java StreetMap p4dataset/*name of chosen map*.txt —-show

Display the map: (directions can be added through the GUI)
java StreetMap p4dataset/*name of chosen map*.txt —-show —-directions

Display the map with preexisting directions given:
java StreetMap p4dataset/*name of chosen map*.txt —-show —-directions i3 i50
A map lauched this way would allow the user to use the GUI.

Work distribution:

	We met up several days before we started writing code, first to think about what was the best implementation and then to divide the work. We did most of the project together. The parts that we did not do together was the implementation of the command line controls and GUI, which Sakhile did, the dijkstras algorithm implementation which Clara took care of, and the README write up which Clara wrote and Sakhile proof read and finished. 

Notes: 
	After the graph is displayed, the algorithm still takes several seconds to display the path (line in red), and even after it does it will display as its found, so the full path is not displayed at first.
	

Extra Credit
The GUI can be controlled using the drop downlists and buttons on it. It also displays the distance between two points. It displays infinity (~ 9e10) when no path is found. Note that when you want to use the GUI, you would have to type the command 
	java StreetMap map.txt --show --directions
Also, the intersections along the path are displayed on the terminal;



















































